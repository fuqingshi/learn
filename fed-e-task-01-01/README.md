1、
* 解决同步模式下，代码任务耗时过长，页面渲染阻塞的问题，优化js执行效率和提升用户体验
* event loop是一个执行模型，同消息队列都是确保异步任务在同步任务执行后继续执行
* 宏任务和微任务都是异步任务执行的两种方式，宏任务产生有如下几种方式：setTimeout、setInterval、setImmediate， 微任务产生有如下方式：Process.nextTick、Promise、Object.observe、MutationObserver，
当微任务队列执行为空时才会执行宏任务队列
