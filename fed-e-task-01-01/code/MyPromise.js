// 判断变量否为function
const isFunction = variable => typeof variable === 'function'

// 定义Promise的三种状态常量
const PENDING = 'PENDING'
const FULFILLED = 'FULFILLED'
const REJECTED = 'REJECTED'

class MyPromise {

  constructor(exeFun) {
    // 构造一个执行器
    if (!isFunction(exeFun)) {
      new TypeError('MyPromise resolver  is not a function')
    }
    // 设置默认状态和默认值
    this._status = PENDING
    this._value = undefined

    // 添加成功回调函数队列
    this._fulfilledQueues = []
      // 添加失败回调函数队列
    this._rejectedQueues = []

    // 执行执行器
    try {
      exeFun(this._resolve.bind(this), this._reject.bind(this))
    } catch (err) {
      this._reject(err)
    }

  }

  // 添加resovle时执行的函数
  _resolve(v) {
      if (this._status !== PENDING) return
        // 依次执行成功队列中的函数，并清空队列
      const run = () => {
          this._status = FULFILLED // 改变为最终状态
            // 依次执行成功队列中的函数，并清空队列
          const runFulfilled = (value) => {
              let cb;
              while (cb = this._fulfilledQueues.shift()) {
                cb(value)
              }
            }
            // 依次执行失败队列中的函数，并清空队列
          const runRejected = (error) => {
              let cb;
              while (cb = this._rejectedQueues.shift()) {
                cb(error)
              }
            }
            /* 如果resolve的参数为Promise对象，则必须等待该Promise对象状态改变后,
        当前Promsie的状态才会改变，且状态取决于参数Promsie对象的状态
      */
          if (v instanceof MyPromise) {
            v.then(value => {
              this._value = value
              runFulfilled(value)
            }, err => {
              this._value = err
              runRejected(err)
            })
          } else {
            this._value = v
            runFulfilled(v)
          }
        }
        // 为了支持同步的Promise，这里采用异步调用
      setTimeout(() => run(), 0)
    }
    // 添加reject时执行的函数
  _reject(err) {
    if (this._status !== PENDING) return
      // 依次执行失败队列中的函数，并清空队列
    const run = () => {
        this._status = REJECTED // 改变为最终状态
        this._value = err
        let cb;
        while (cb = this._rejectedQueues.shift()) {
          cb(err)
        }
      }
      // 为了支持同步的Promise，这里采用异步调用
    setTimeout(run, 0)
  }

  // 添加then 方法
  then(onFulfilled, onRejected) {
    const { _value, _status } = this

    // 返回一个Promise
    return new MyPromise((onFulfilledNext, onRejectedNext) => {
      // 封装成功返回函数
      let fulfilled = value => {
        try {
          // 判断传过来的参数是否为函数
          if (!isFunction(onFulfilled)) {
            // 是，直接执行
            onFulfilledNext(value)
          } else {
            // 不是则调用then的resolve
            let res = onFulfilled(value)
            if (res instanceof MyPromise) {
              // 如果返回是MyPromise，必须等待其状态改变后在执行下一个回调
              res.then(onFulfilledNext, onRejectedNext)
            } else {
              //否则会将返回结果直接作为参数，传入下一个then的回调函数，并立即执行下一个then的回调函数
              onFulfilledNext(res)
            }
          }
        } catch (err) {
          // 出错了，新promise对象状态为失败
          onRejectedNext(err)
        }
      }

      let rejected = error => {
        try {
          // 判断传过来的参数是否为函数
          if (!isFunction(onRejected)) {
            // 是，直接执行
            onRejectedNext(error)
          } else {
            // 不是则调用then的ejected
            let res = onRejected(error)
            if (res instanceof MyPromise) {
              // 如果返回是MyPromise，必须等待其状态改变后在执行下一个回调
              res.then(onFulfilledNext, onRejectedNext)
            } else {
              //否则会将返回结果直接作为参数，传入下一个then的回调函数，并立即执行下一个then的回调函数
              onRejectedNext(res)
            }
          }
        } catch (err) {
          // 出错了，新promise对象状态为失败
          onRejectedNext(err)
        }
      }

      switch (_status) {
        // 当状态为padding。将then方法回调函数放入执行队列，等待执行
        case PENDING:
          this._fulfilledQueues.push(fulfilled)
          this._rejectedQueues.push(rejected)
          break
          // 状态被改变的时候，立即执行对应回调函数
        case FULFILLED:
          fulfilled(_value)
          break
        case REJECTED:
          rejected(_value)
          break
      }
    })
  }


  // 添加catch方法
  catch (onRejected) {
    return this.then(undefined, onRejected)
  }
  //添加finally方法，用于指定不管 Promise 对象最后状态如何，都会执行的操作
  finally (cb) {
    return this.then(
      value  => MyPromise.resolve(cb()).then(() => value),
      reason => MyPromise.resolve(cb()).then(() => { throw reason })
    );
  };

  // 添加静态resolve方法
  static resolve(value) {
    // 如果参数是MyPromise实例，直接返回这个实例
    if (value instanceof MyPromise) return value
    // 不是就new一个MyPromise
    return new MyPromise(resolve => resolve(value))
  }

  // 添加静态reject方法
  static reject (value) {
    return new MyPromise((resolve ,reject) => reject(value))
  }

  // 添加静态all方法
  static all (list) {
    return new MyPromise((resolve, reject) => {
      /**
       * 返回值的集合
       */
      let values = []
      let count = 0
      for (let [i, p] of list.entries()) {
        // 数组参数如果不是MyPromise实例，先调用MyPromise.resolve
        this.resolve(p).then(res => {
          values[i] = res
          count++
          // 所有状态都变成fulfilled时返回的MyPromise状态就变成fulfilled
          if (count === list.length) resolve(values)
        }, err => {
          // 有一个被rejected时返回的MyPromise状态就变成rejected
          reject(err)
        })
      }
    })
  }
  // 添加静态race方法
  static race (list) {
    return new MyPromise((resolve, reject) => {
      for (let p of list) {
        // 只要有一个实例率先改变状态，新的MyPromise的状态就跟着改变
        this.resolve(p).then(res => {
          resolve(res)
        }, err => {
          reject(err)
        })
      }
    })
  }
}

module.exports = MyPromise
