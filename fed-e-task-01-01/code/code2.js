const fp = require('lodash/fp')
// horsepower 马力，dollar_value 加个， in_stock 库存
const cars= [
    { name: 'Ferrari FF', horsepower: 660, dollar_value: 700000, in_stock: true },
    { name: 'Spyker C12 Zagato', horsepower: 650, dollar_value: 648000, in_stock: false },
    { name: 'Jaguar XKR-S', horsepower: 525, dollar_value: 114200, in_stock: false },
    { name: 'Audi R8', horsepower: 660, dollar_value: 700000, in_stock: false },
    { name: 'Aston Martin One-77', horsepower: 750, dollar_value: 1850000, in_stock: true },
    { name: 'Pagani Huayra', horsepower: 700, dollar_value: 1300000, in_stock: false },
]

// 练习 1
// let f = fp.flowRight(fp.prop('in_stock'), fp.first, fp.reverse)
// console.log('最后一个十分有库存:' + f(cars))
let f2 = fp.flowRight(fp.prop('in_stock'), fp.last)
console.log('最后一个十分有库存:' + f2(cars))

// 练习2

let f3 = fp.flowRight(fp.prop('name'), fp.first)
console.log('第一个car 的name:' + f3(cars))


// 练习3
let _average = xs => fp.reduce(fp.add, 0, xs) / xs.length
// let avg = function (cars) {
//     let do11 = fp.map(function(car){
//         return car.dollar_value
//     }, cars)
//     return _average(do11)
// }
// console.log(avg(cars))
let _getDollar = car => car.dollar_value
let f = fp.flowRight(_average, fp.map(_getDollar))
console.log(f(cars))

// 练习4
let _underscore = fp.replace(/\W+/g, '_')
let sanitizeNames = fp.flowRight(fp.map(_underscore))
console.log(sanitizeNames(["312 sd", "312 sd"]))
