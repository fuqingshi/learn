const fp = require('lodash/fp')
const { Maybe, Container } = require('./support')


// 练习1
let maybe = Maybe.of([5, 6, 1])
let ex1 = addNum => {
    return maybe.map(d => fp.map(x =>  fp.add(x,addNum), d))
}
console.log('加空：', ex1())
console.log('加2：', ex1(2))

// 练习2
console.log('=====================ex2')
let xs = Container.of(['do', 'ray', 'me', 'fa', 'so', 'la', 'ti', 'do'])
let ex2 = () => {
    return xs.map(d => fp.first(d))
}
console.log(ex2())

// 练习3
console.log('=====================ex3')
let  user = { id : '2', name: 'Albert' }
let safeProp = fp.curry(function(x, o) {
    return Maybe.of(o[x])
})

let ex3 = (obj, prop) => {
    let mb = safeProp(prop)(obj)
    return mb.map(d => d.split(''))
        .map(d => fp.first(d))
}
console.log(ex3(user, 'name'))



// 练习4
console.log('=====================ex4')
let ex4 = function (n) {
    return Maybe.of(n)
    .map(x => parseInt(x))
}
let xx = undefined
console.log(ex4(xx))
xx = null
console.log(ex4(xx))
xx = '23'
console.log(ex4(xx))

